#include "gtest/gtest.h"
#include "scrabble-score.hpp"

struct ScrabbleScoreTestSuite {};

TEST(ScrabbleScoreTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(ScrabbleScoreTestSuite, NoWord)
{
  ASSERT_EQ(computeScrabbleScore(""), 0);
}

TEST(ScrabbleScoreTestSuite, OnePointLetters)
{
  ASSERT_EQ(computeScrabbleScore("AEIOULNRST"), 10);
}

TEST(ScrabbleScoreTestSuite, SmallLetters)
{
  ASSERT_EQ(computeScrabbleScore("aeiou"), 5);
}

TEST(ScrabbleScoreTestSuite, TwoPointLetters)
{
  ASSERT_EQ(computeScrabbleScore("DG"), 4);
}

TEST(ScrabbleScoreTestSuite, ThreePointLetters)
{
  ASSERT_EQ(computeScrabbleScore("BCMP"), 12);
}

TEST(ScrabbleScoreTestSuite, FourPointLetters)
{
  ASSERT_EQ(computeScrabbleScore("FHVWY"), 20);
}

TEST(ScrabbleScoreTestSuite, FivePointLetters)
{
  ASSERT_EQ(computeScrabbleScore("K"), 5);
}

TEST(ScrabbleScoreTestSuite, EightPointLetters)
{
  ASSERT_EQ(computeScrabbleScore("JX"), 16);
}

TEST(ScrabbleScoreTestSuite, TenPointLetters)
{
  ASSERT_EQ(computeScrabbleScore("QZ"), 20);
}

TEST(ScrabbleScoreTestSuite, MultiplyLetters)
{
  ASSERT_EQ(computeScrabbleScore("QZ", 3*2), 120);
}

TEST(ScrabbleScoreTestSuite, CabbageTest)
{
  ASSERT_EQ(computeScrabbleScore("cabbage"), 14);
}



