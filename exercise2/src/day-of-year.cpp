#include "day-of-year.hpp"
#include <ctime>
int dayOfYear(int month, int dayOfMonth, int year) {
    tm t{};
    t.tm_mon = month - 1;
    t.tm_mday = dayOfMonth;
    t.tm_year = year - 1900;
    auto sec = mktime(&t);
    return localtime(&sec)->tm_yday + 1;
}
