#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};
TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}
TEST(DayOfYearTestSuite, January1stIs1stDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}
TEST(DayOfYearTestSuite, February1stIs32ndDayOfYear)
{
    ASSERT_EQ(dayOfYear(2, 1, 2020), 32);
}
TEST(DayOfYearTestSuite, March1stIs61stDayOfYear)
{
    ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}
TEST(DayOfYearTestSuite, LastDecember)
{
    ASSERT_EQ(dayOfYear(12, 31, 2019), 365);
}
TEST(DayOfYearTestSuite, LeepYearCheck)
{
    ASSERT_EQ(dayOfYear(12, 31, 2020), 366);
}
